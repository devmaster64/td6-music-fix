// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include "detours.h"

#include "FunctionOverrides.h"
#include "TestDrive6.h"
#include "dllmain.h"

void Init()
{
	TestDrive6::SetupLogger();
	Logger << LOG_INFO << "Init(): Version 1.0" << std::endl;
	Logger << LOG_INFO << "Init(): Music fix DLL injected" << std::endl;
}

void AddDetours()
{
	DetourRestoreAfterWith();
	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourAttach(&(PVOID&)RealFunctions::CreateFile, OverridenFunctions::CreateFile);
	DetourAttach(&(PVOID&)RealFunctions::CloseHandle, OverridenFunctions::CloseHandle);
	DetourTransactionCommit();

	Logger << LOG_INFO << "AddDetours(): Detours added" << std::endl;

	auto handle = GetActiveWindow();
	ShowWindow(handle, SW_RESTORE);
}

void RemoveDetours()
{
	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());
	DetourDetach(&(PVOID&)RealFunctions::CreateFile, OverridenFunctions::CreateFile);
	DetourDetach(&(PVOID&)RealFunctions::CloseHandle, OverridenFunctions::CloseHandle);
	DetourTransactionCommit();
	Logger << LOG_INFO << "RemoveDetours(): Detours removed" << std::endl;
	Logger << LOG_INFO << "RemoveDetours(): Exiting..." << std::endl << std::endl;
}

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		Init();
		AddDetours();
	}
	else if (ul_reason_for_call == DLL_PROCESS_DETACH)
		RemoveDetours();

	return TRUE;
}

#define WM_COLLECT_MUSIC_FILES (WM_USER + 0x0001)

extern "C" __declspec(dllexport) int NextHook(int code, WPARAM wParam, LPARAM lParam) {
	auto info = (CWPRETSTRUCT*)lParam;
	if (info->lParam == WM_COLLECT_MUSIC_FILES)
		TestDrive6::CollectMusicFiles();
	return CallNextHookEx(NULL, code, wParam, lParam);
}