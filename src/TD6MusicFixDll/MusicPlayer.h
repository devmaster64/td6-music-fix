#pragma once

#include "pch.h"
#include "strsafe.h"
#include <atomic>

using namespace winrt;
using namespace Windows::Storage;
using namespace Windows::Foundation;
using namespace Windows::Media::Core;
using namespace Windows::Media::Playback;
using namespace Windows::Foundation::Collections;


struct MusicPlayer {
private:
	inline static MusicPlayer* _instance = 0;
	inline static std::atomic_bool _block = false;

	MediaPlayer _mediaPlayer{ nullptr };
	MediaPlaybackList _shuffleList{ nullptr };

	wchar_t _currentlyPlaying[BUFSIZ];

	MusicPlayer() {
		_mediaPlayer = MediaPlayer();
		_mediaPlayer.Volume(0.0);
		_mediaPlayer.MediaEnded({ this, &MusicPlayer::MediaEndedHandler });

		_shuffleList = MediaPlaybackList();
	}

	void MediaEndedHandler(MediaPlayer player, const IInspectable& value)
	{
		PlayShuffleList();
	}
public:
	static MusicPlayer* GetInstance() {
		if (!_instance)
			_instance = new MusicPlayer;
		return _instance;
	}

	void AddMusicFiles(IVectorView<StorageFile> files) {
		_block = true;
		for (auto file : files)
		{
			Logger << LOG_INFO << "AddMusicFiles(): Adding music file: " << file.Name().c_str() << std::endl;	
			auto mediaSource = MediaSource::CreateFromStorageFile(file);
			auto item = MediaPlaybackItem(mediaSource);
			_shuffleList.Items().Append(item);
		}
		_shuffleList.AutoRepeatEnabled(true);
		_shuffleList.ShuffleEnabled(true);
		Sleep(2000);
		Stop();
		_mediaPlayer.Volume(0.7);
		_block = false;
	}

	void PlayShuffleList()
	{
		_mediaPlayer.Source(_shuffleList);
		_shuffleList.MoveNext();
	}

	void Play(wchar_t* file) {
		if (_block.load())
			return;
		
		if (!lstrcmpiW(_currentlyPlaying, file))
			return;
		Stop();
		Logger << LOG_INFO << "Play(): Playing music file: " << file << std::endl;
		auto storageFile = StorageFile::GetFileFromPathAsync(file).get();
		_mediaPlayer.Source(MediaSource::CreateFromStorageFile(storageFile));
		_mediaPlayer.Play();
		StringCchCopyW(_currentlyPlaying, BUFSIZ, file);
	}

	void Stop() {
		Logger << LOG_INFO << "Stop(): Stopping all music players" << std::endl;

		if (_mediaPlayer.CurrentState() != MediaPlayerState::Paused && _mediaPlayer.CanPause())
			_mediaPlayer.Pause();
	}
};