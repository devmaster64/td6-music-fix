#pragma once

#include "pch.h"
#include "Utils.h"
#include "TestDrive6.h"

using namespace Utils;

struct RealFunctions {
    static inline HANDLE(WINAPI* CreateFile)
        (LPCWSTR lpFileName, DWORD dwDesiredAccess, DWORD dwShareMode, LPSECURITY_ATTRIBUTES lpSecurityAttributes, 
            DWORD dwCreationDisposition, DWORD dwFlagsAndAttributes, HANDLE hTemplateFile) = CreateFileW;

    static inline BOOL(WINAPI* CloseHandle)(HANDLE hwnd) = CloseHandle;
};

struct OverridenFunctions {

    static inline HANDLE WINAPI CreateFile(LPCWSTR lpFileName, DWORD dwDesiredAccess, DWORD dwShareMode, LPSECURITY_ATTRIBUTES lpSecurityAttributes,
        DWORD dwCreationDisposition, DWORD dwFlagsAndAttributes, HANDLE hTemplateFile)
    {
        auto handle = RealFunctions::CreateFile(lpFileName, dwDesiredAccess, dwShareMode, lpSecurityAttributes, dwCreationDisposition,
            dwFlagsAndAttributes, hTemplateFile);

        TestDrive6::ResourceFileHandleOpened(lpFileName, handle);

        return handle;
    }

    static inline BOOL WINAPI CloseHandle(HANDLE hwnd)
    {
        TestDrive6::ResourceFileHandleClosed(hwnd);
        return RealFunctions::CloseHandle(hwnd);
    }
};