#pragma once

#include "pch.h"
#include "MusicPlayer.h"
#include "Utils.h"

using namespace Utils;

struct TestDrive6 {
public:
	inline static winrt::fire_and_forget CollectMusicFiles()
	{
		Logger << LOG_INFO << "CollectMusicFiles(): Collecting music files" << std::endl;
		wchar_t pwd[FILENAME_MAX];
		GetCurrentDirectory(FILENAME_MAX, pwd);
		std::wstring currentDir(pwd);
		Logger << LOG_INFO << "CollectMusicFiles(): Current working directory: " << currentDir << std::endl;

		auto musicDir = currentDir + L"\\music";
		Logger << LOG_INFO << "CollectMusicFiles(): Music directory: " << musicDir << std::endl;

		auto musicFolder = co_await StorageFolder::GetFolderFromPathAsync(musicDir);
		auto musicFiles = co_await musicFolder.GetFilesAsync();
		MusicPlayer::GetInstance()->AddMusicFiles(musicFiles);		
	}

	inline static void ResourceFileHandleOpened(std::wstring file, HANDLE handle) {
		try
		{
			if (handle != nullptr && (file.empty() || file.length() < 5))
				return;

			Strings::ToLower(file);
			if (Strings::EndsWith(file, L".wma"))
			{
				WCHAR full_path[BUFSIZ];
				GetFinalPathNameByHandle(handle, full_path, BUFSIZ, VOLUME_NAME_DOS);
				MusicPlayer::GetInstance()->Play(&full_path[4]);
			}
			else if (Strings::EndsWith(file, L"ffcars.bik") || Strings::EndsWith(file, L"credits.bik"))
			{
				Logger << LOG_INFO << "Playing video, stopping music" << std::endl;
				MusicPlayer::GetInstance()->Stop();
			}
		}
		catch (const std::exception& e)
		{
			Logger << LOG_ERR << "Exception: " << e.what() << std::endl;
		}
	}

	inline static void ResourceFileHandleClosed(HANDLE handle) {
		if (handle == nullptr)
			return;

		wchar_t full_path[BUFSIZ];
		auto status = GetFinalPathNameByHandle(handle, full_path, BUFSIZ, VOLUME_NAME_DOS);
		if (!status)
			return;

		std::wstring file(&full_path[4]);
		Strings::ToLower(file);
		if (Strings::EndsWith(file, L"ffcars.bik") || Strings::EndsWith(file, L"credits.bik"))
		{
			Logger << LOG_INFO << "Video stopped, playing music" << std::endl;
			MusicPlayer::GetInstance()->PlayShuffleList();
		}
	}

	inline static void SetupLogger() {
		Logger.open("TD6mf.log", std::ios::app);
	}
};