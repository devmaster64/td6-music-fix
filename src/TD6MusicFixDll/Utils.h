#pragma once

#include "pch.h"

#include <iostream>
#include <fstream>

std::wfstream Logger;

#define LOG_INFO std::unitbuf << "[INF] "
#define LOG_ERR std::unitbuf << "[ERR] "

namespace Utils {
	struct Strings {
		inline static void ToLower(std::wstring& str) {
			std::transform(str.begin(), str.end(), str.begin(), ::tolower);
		}

		inline static bool EndsWith(std::wstring str, std::wstring str2) {
			auto strLen = str.length();
			auto str2Len = str2.length();
			if (str2Len > strLen)
				return false;

			auto subStr = str.substr(strLen - str2Len, str2Len);
			return subStr.compare(str2) == 0;
		}

		inline static bool Contains(std::wstring str1, std::wstring str2) {
			return str1.find(str2) != std::wstring::npos;
		}
	};
}