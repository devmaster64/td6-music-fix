#include <Windows.h>
#include <iostream>

#define WM_COLLECT_MUSIC_FILES (WM_USER + 0x0001)

INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
    PSTR lpCmdLine, INT nCmdShow)
{
    HMODULE dll = LoadLibraryEx(L"TD6mf.dll", NULL, DONT_RESOLVE_DLL_REFERENCES);
    if (dll == NULL) {
        std::cout << "[ FAILED ] The DLL could not be found." << std::endl;
        system("pause");
        return EXIT_FAILURE;
    }

    HOOKPROC addr = (HOOKPROC)GetProcAddress(dll, "NextHook");
    if (addr == NULL) {
        std::cout << "[ FAILED ] The function was not found." << std::endl;
        system("pause");
        return EXIT_FAILURE;
    }

    STARTUPINFO info = { sizeof(info) };
    info.wShowWindow = SW_MINIMIZE;
    PROCESS_INFORMATION processInfo;
    if (!CreateProcess(L"td6.exe", NULL, NULL, NULL, TRUE, 0, NULL, NULL, &info, &processInfo))
    {
        std::cout << "[ FAILED ] Could not start Test Drive 6" << std::endl;
        system("pause");
    }

    auto tries = 0;
    HWND hwnd;
    do
    {
        hwnd = FindWindow(NULL, L"Test Drive 6 Properties");
        if(hwnd == NULL)
            Sleep(100);
    } while (hwnd == NULL && ++tries < 10);

    ShowWindow(hwnd, SW_MINIMIZE);

    if (tries > 9)
    {
        std::cout << "[ FAILED ] Hooking failed" << std::endl;
    }

    DWORD pid = NULL;
    DWORD tid = GetWindowThreadProcessId(hwnd, &pid);
    if (tid == NULL) {
        std::cout << "[ FAILED ] Could not get thread ID of the target window." << std::endl;
        system("pahwnduse");
        return EXIT_FAILURE;
    }

    HHOOK handle = SetWindowsHookEx(WH_GETMESSAGE, addr, dll, tid);
    if (handle == NULL) {
        std::cout << "[ FAILED ] Couldn't set the hook with SetWindowsHookEx." << std::endl;
        system("pause");
        return EXIT_FAILURE;
    }

    PostMessage(hwnd, WM_COLLECT_MUSIC_FILES, 0, 0);
    Sleep(3000);
    ShowWindow(hwnd, SW_SHOWDEFAULT);

    std::cout << "[ INFO ] Music fix DLL injected" << std::endl;
    std::cout << "[ INFO ] Do not close this window!!!" << std::endl;

    WaitForSingleObject(processInfo.hProcess, INFINITE);
    CloseHandle(processInfo.hProcess);
    CloseHandle(processInfo.hThread);
    return EXIT_SUCCESS;
}