# TD6 Music fix for Windows 10
This project uses Microsoft Detours and WinRT to add the music playback back into the game.

# Software Requirements
This works only on Windows 10 10240 and above. Older version of Windows will not work.

# Build
Tools and softwares you'll need:
  * Visual Studio 2019
  * Windows 10 SDK 10.0.18362

Open the solution in Visual Studio and change the configuration to `Release` and build the solution.

The output will be in the `out` folder in the root of the repo.

# Download
Download from: https://gitlab.com/devmaster64/td6-music-fix/-/tags

# Usage
Simply copy `TD6mf.dll` and `TD6mf.exe` to the root of `Test Drive 6`'s installation folder and run `TD6mf.exe`. That's it!
